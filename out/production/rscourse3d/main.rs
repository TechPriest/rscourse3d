#![feature(const_fn)]

#![feature(alloc_system)]
extern crate alloc_system;

extern crate byteorder;
extern crate time;
extern crate rand;

extern crate png_encode_mini;

mod buffer;
mod canvas;
mod geometry;
mod mesh;
mod objparser;
mod experiments;

use std::io::{BufReader, BufWriter};
use canvas::{Color, Canvas};
use png_encode_mini::write_rgba_from_u8;


const CLEAR_COLOR: Color = Color {
    r: 50,
    g: 50,
    b: 50,
    a: 255,
};


fn run_experiment<F>(experiment: F, image_width: usize, image_height: usize, name: &str)
    where F: Fn(&mut Canvas)
{
    use std::sync::atomic::{AtomicUsize, Ordering};
    static COUNTER: AtomicUsize = AtomicUsize::new(0);

    let mut canvas = Canvas::new(image_width, image_height);
    canvas.clear_color(&CLEAR_COLOR);

    let start_time = time::precise_time_s();
    experiment(&mut canvas);
    let end_time = time::precise_time_s();

    println!("{0} finished in {1}s", name, end_time - start_time);

    let n = COUNTER.fetch_add(1, Ordering::SeqCst) + 1;

    let file_name = format!("{0:02}_{1}.png", n, name);
    let f = std::fs::File::create(file_name).unwrap();
    let mut writer = BufWriter::new(f);

    unsafe {
        let pixel_data = &canvas.color_data()[0] as *const _ as *const u8;
        let bytes = std::slice::from_raw_parts(pixel_data, canvas.width() * canvas.height() * 4);
        write_rgba_from_u8(&mut writer,
                           bytes,
                           canvas.width() as u32,
                           canvas.height() as u32)
            .unwrap();
    }
}

fn run_experiment_1<F, A>(experiment: F,
                          a: &A,
                          image_width: usize,
                          image_height: usize,
                          name: &str)
    where F: Fn(&mut Canvas, &A)
{
    run_experiment(|mut canvas| {
                       experiment(&mut canvas, &a);
                   },
                   image_width,
                   image_height,
                   name);
}

fn main() {
    run_experiment(experiments::per_pixel_line,
                   200usize,
                   200usize,
                   "per_pixel_line");

    run_experiment(experiments::lines, 200usize, 200usize, "lines");

    run_experiment(experiments::triangle, 300usize, 300usize, "triangle");


    let f = std::fs::File::open("african_head.obj").unwrap();
    let model = objparser::parse_obj(BufReader::new(f));

    run_experiment_1(experiments::wireframe,
                     &model,
                     800usize,
                     800usize,
                     "wireframe");

    run_experiment_1(experiments::random_colored_tris,
                     &model,
                     800usize,
                     800usize,
                     "random_colored_tris");

    run_experiment_1(experiments::simple_flat_lit,
                     &model,
                     800usize,
                     800usize,
                     "simple_flat_lit");

    run_experiment_1(experiments::simple_flat_lit_zbuffer,
                     &model,
                     800usize,
                     800usize,
                     "simple_flat_lit_zbuffer");
}
