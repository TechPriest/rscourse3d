#![allow(dead_code)]


#[derive(Default, Copy, Clone, Debug)]
pub struct Vector3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Vector3 {
    pub fn new(x: f32, y: f32, z: f32) -> Vector3 {
        Vector3 { x: x, y: y, z: z }
    }

    pub fn squared_length(&self) -> f32 {
        (self.x * self.x) + (self.y * self.y) + (self.z * self.z)
    }

    pub fn length(&self) -> f32 {
        self.squared_length().sqrt()
    }

    pub fn normalize(v: Vector3) -> Vector3 {
        let l = v.length();
        assert!(l != 0f32);
        Vector3 {
            x: v.x / l,
            y: v.y / l,
            z: v.z / l,
        }
    }

    pub fn dot(a: Vector3, b: Vector3) -> f32 {
        (a.x * b.x) + (a.y * b.y) + (a.z * b.z)
    }

    pub fn cross(a: Vector3, b: Vector3) -> Vector3 {
        Vector3 {
            x: (a.y * b.z) - (a.z * b.y),
            y: (a.z * b.x) - (a.x * b.z),
            z: (a.x * b.y) - (a.y * b.x),
        }
    }
}

impl PartialEq for Vector3 {
    fn eq(&self, other: &Vector3) -> bool {
        (self.x == other.x) && (self.y == other.y) && (self.z == other.z)
    }

    fn ne(&self, other: &Vector3) -> bool {
        !self.eq(other)
    }
}

impl Eq for Vector3 {}

use std::ops::{Add, Sub, Mul, Div};

impl Add for Vector3 {
    type Output = Vector3;
    fn add(self, other: Vector3) -> Vector3 {
        Vector3 {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl Sub for Vector3 {
    type Output = Vector3;
    fn sub(self, other: Vector3) -> Vector3 {
        Vector3 {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl Mul<f32> for Vector3 {
    type Output = Vector3;
    fn mul(self, x: f32) -> Vector3 {
        Vector3 {
            x: self.x * x,
            y: self.y * x,
            z: self.z * x,
        }
    }
}

impl Div<f32> for Vector3 {
    type Output = Vector3;
    fn div(self, x: f32) -> Vector3 {
        let inv_x = 1.0f32 / x;
        Vector3 {
            x: self.x * inv_x,
            y: self.y * inv_x,
            z: self.z * inv_x,
        }
    }
}
