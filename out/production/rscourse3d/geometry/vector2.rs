#![allow(dead_code)]

#[derive(Default, Copy, Clone, Debug)]
pub struct Vector2 {
    pub x: f32,
    pub y: f32,
}

impl Vector2 {
    pub fn new(x: f32, y: f32) -> Vector2 {
        Vector2 { x: x, y: y }
    }
}

impl PartialEq for Vector2 {
    fn eq(&self, other: &Vector2) -> bool {
        (self.x == other.x) && (self.y == other.y)
    }

    fn ne(&self, other: &Vector2) -> bool {
        !self.eq(other)
    }
}

impl Eq for Vector2 {}

use std::ops::{Add, Sub, Mul, Div};

impl Add for Vector2 {
    type Output = Vector2;
    fn add(self, other: Vector2) -> Vector2 {
        Vector2 {
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl Sub for Vector2 {
    type Output = Vector2;
    fn sub(self, other: Vector2) -> Vector2 {
        Vector2 {
            x: self.x - other.x,
            y: self.y - other.y,
        }
    }
}

impl Mul<f32> for Vector2 {
    type Output = Vector2;
    fn mul(self, x: f32) -> Vector2 {
        Vector2 {
            x: self.x * x,
            y: self.y * x,
        }
    }
}

impl Div<f32> for Vector2 {
    type Output = Vector2;
    fn div(self, x: f32) -> Vector2 {
        let inv_x = 1.0f32 / x;
        Vector2 {
            x: self.x * inv_x,
            y: self.y * inv_x,
        }
    }
}
