

pub struct Matrix4x3 {
    data: [f32; 4 * 3],
}

impl Matrix4x3 {
    fn identity() -> Matrix4x3 {
        Matrix4x3 { data: [0f32, 0f32, 0f32, 0f32, 0f32, 0f32, 0f32, 0f32, 0f32, 0f32, 0f32, 0f32] }
    }
}