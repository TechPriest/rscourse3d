use geometry::{Vector2, Vector3};

#[derive(Default, Copy, Clone, Debug)]
pub struct Vertex {
    pub coord: Vector3,
    pub uv: Vector2,
    pub normal: Vector3,
}

#[derive(Default, Copy, Clone, Debug)]
pub struct Face {
    pub a: i32,
    pub b: i32,
    pub c: i32,
}
