#![allow(dead_code)]

use std::io::prelude::*;
use std::num;
use std::fmt;
use std::error::Error;
use std::result;
use std::str::FromStr;
use std::default::Default;
use super::geometry::{Vector2, Vector3};
use super::mesh::Face;

#[derive(Debug)]
pub enum ObjParseError<E: Error> {
    Io(E),
    Parse(E),
    InvalidInputFormat,
}

type Result<T, E> = result::Result<T, ObjParseError<E>>;


impl<E: Error> fmt::Display for ObjParseError<E> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ObjParseError::Io(ref err) => write!(f, "IO error: {}", err),
            ObjParseError::Parse(ref err) => write!(f, "Parse error: {}", err),
            ObjParseError::InvalidInputFormat => write!(f, "Invalid input format"),
        }
    }
}

impl<E: Error> Error for ObjParseError<E> {
    fn description(&self) -> &str {
        match *self {
            ObjParseError::Io(ref err) => err.description(),
            ObjParseError::Parse(ref err) => err.description(),
            ObjParseError::InvalidInputFormat => "Invalid input format",
        }
    }

    fn cause(&self) -> Option<&Error> {
        match *self {
            ObjParseError::Io(ref err) => Some(err),
            ObjParseError::Parse(ref err) => Some(err),
            ObjParseError::InvalidInputFormat => None,
        }
    }
}



pub struct ObjModelData {
    pub vertices: Vec<Vector3>,
    pub faces: Vec<Face>,
}

const COMMENT_PREFIX: &'static str = "#";
const VERTEX_PREFIX: &'static str = "v ";
const FACE_PREFIX: &'static str = "f ";
const NORMAL_PREFIX: &'static str = "vn ";
const TEXCOORD_PREFIX: &'static str = "vt ";

/// Checks if specified line from OBJ file is a comment.
fn is_comment(s: &str) -> bool {
    &s[0..COMMENT_PREFIX.len()] == COMMENT_PREFIX
}

/// Extracts substring with vertex coordinates only, without prefix.
fn get_vertex_coord_substr(s: &str) -> &str {
    &s[VERTEX_PREFIX.len()..s.len()]
}

/// Extracts substring with texture coordinates only, without prefix
fn get_texcoord_data_substr(s: &str) -> &str {
    &s[TEXCOORD_PREFIX.len()..s.len()]
}

/// Extracts substring with face data only, without prefix
fn get_face_data_substr(s: &str) -> &str {
    &s[FACE_PREFIX.len()..s.len()]
}

#[test]
fn detects_comment() {
    let input = "#v 12.34 42.42 66.99";
    assert!(is_comment(&input));
    let input = "v 12.34 42.42 66.99";
    assert!(!is_comment(&input));
}

pub fn parse_obj<R: BufRead>(src: R) -> ObjModelData {
    // https://en.wikipedia.org/wiki/Wavefront_.obj_file

    let mut vertices: Vec<Vector3> = Default::default();
    let mut faces: Vec<Face> = Default::default();

    for line in src.lines() {
        let line = match line {
            Ok(line) => line,
            Err(_) => break,
        };

        if line.len() == 0 {
            continue;
        }

        if is_comment(&line) {
            continue;
        }

        match &line[0..2] {
            VERTEX_PREFIX => {
                vertices.push(parse_coord(get_vertex_coord_substr(&line)).unwrap());
            }
            FACE_PREFIX => faces.push(parse_face(get_face_data_substr(&line)).unwrap()),
            "g " => (),
            "s " => (),
            _ => {
                match &line[0..3] {
                    TEXCOORD_PREFIX => (),
                    NORMAL_PREFIX => (),
                    "vp " => (),
                    _ => panic!("Unexpected line format {:?} {:?}", line, &line[0..3]),
                }
            }
        };
    }

    ObjModelData {
        vertices: vertices,
        faces: faces,
    }
}

/// Returns first non-empty substring
fn get_next_nonempty_substr<'a>(iter: &mut Iterator<Item = &'a str>) -> Option<&'a str> {
    loop {
        match iter.next() {
            None => return None,
            Some(s) => {
                if s.len() > 0 {
                    return Some(s);
                }
            }
        }
    }
}

/// Parses next float value in input stream
fn parse_next<T: FromStr>(iter: &mut Iterator<Item = &str>) -> Result<T, T::Err>
    where T::Err: Error
{
    let s = try!(get_next_nonempty_substr(iter).ok_or(ObjParseError::InvalidInputFormat));
    match T::from_str(s) {
        Ok(x) => return Ok(x),
        Err(e) => return Err(ObjParseError::Parse(e)),
    }
}

/// Parses vertex definition in text OBJ format.
fn parse_coord(data: &str) -> Result<Vector3, num::ParseFloatError> {
    // Input example "0.123 0.234 0.345 1.0"
    let mut iter = data.split_whitespace();
    Ok(Vector3 {
        x: try!(parse_next::<f32>(&mut iter)),
        y: try!(parse_next::<f32>(&mut iter)),
        z: try!(parse_next::<f32>(&mut iter)),
    })
}

/// Parses UV-coordinate for vertex
fn parse_texcoord(data: &str) -> Result<Vector2, num::ParseFloatError> {
    // Input example "0.535 0.917 0.000"
    let mut iter = data.split_whitespace();
    Ok(Vector2 {
        x: try!(parse_next::<f32>(&mut iter)),
        y: try!(parse_next::<f32>(&mut iter)),
    })
}

#[derive(Default, Debug, Copy, Clone)]
struct FaceIndices {
    coord: i32,
    tex_coord: i32,
    normal: i32,
}

fn parse_face_indices(s: &str) -> result::Result<FaceIndices, ObjParseError<num::ParseIntError>> {
    // Imput example "460/450/460"
    let mut iter = s.split('/');
    Ok(FaceIndices {
        coord: try!(parse_next::<i32>(&mut iter)),
        tex_coord: try!(parse_next::<i32>(&mut iter)),
        normal: try!(parse_next::<i32>(&mut iter)),
    })
}

#[test]
fn parses_face_indices() {
    let input = "460/450/460";
    let f = parse_face_indices(&input).unwrap();
    assert!(f.coord == 460);
    assert!(f.tex_coord == 450);
    assert!(f.normal == 460);
}


/// Parses face indices
fn parse_face(data: &str) -> Result<Face, num::ParseIntError> {
    // Imput example "460/450/460 260/242/260 29/244/29"
    let mut iter = data.split_whitespace();

    let parse_next_coord =
        |mut iter: &mut Iterator<Item = &str>| -> Result<i32, num::ParseIntError> {
            let s = try!(get_next_nonempty_substr(&mut iter)
                .ok_or(ObjParseError::InvalidInputFormat));
            let f = try!(parse_face_indices(s));
            Ok(f.coord)
        };

    Ok(Face {
        a: try!(parse_next_coord(&mut iter)),
        b: try!(parse_next_coord(&mut iter)),
        c: try!(parse_next_coord(&mut iter)),
    })
}

#[test]
fn parses_face_data() {
    let input = "460/450/460 260/242/260 29/244/29";
    let f = parse_face(input).unwrap();

    assert!(f.a == 460);
    assert!(f.b == 260);
    assert!(f.c == 29);
}

#[test]
fn parses_vertex() {
    let input = "v   -0.000581696 -0.734665 -0.623267";
    let v = parse_coord(get_vertex_coord_substr(&input)).unwrap();

    assert!(v.x == -0.000581696f32);
    assert!(v.y == -0.734665f32);
    assert!(v.z == -0.623267f32);
}

#[test]
fn fails_to_parse_coord_on_insufficient_data() {
    let input = "v 12.34 42.42";
    let r = parse_coord(get_vertex_coord_substr(&input));
    assert!(r.is_err());
}

#[test]
fn fails_to_parse_coord_on_malformed_data() {
    let input = "v zzz1sd2.34 xxx42.42 yyy66.99";
    let r = parse_coord(get_vertex_coord_substr(&input));
    assert!(r.is_err());
}

#[test]
fn parses_texture_coord() {
    let input = "vt  0.535 0.917 0.000";
    let uv = parse_texcoord(get_texcoord_data_substr(&input)).unwrap();

    assert!(uv.x == 0.535);
    assert!(uv.y == 0.917);
}
