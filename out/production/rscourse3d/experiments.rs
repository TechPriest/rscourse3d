use canvas::*;
use geometry::*;
use objparser::ObjModelData;

const WHITE_COLOR: Color = Color {
    r: 255,
    g: 255,
    b: 255,
    a: 255,
};

const RED_COLOR: Color = Color {
    r: 255,
    g: 0,
    b: 0,
    a: 255,
};


pub fn per_pixel_line(canvas: &mut Canvas) {
    assert!(canvas.height() <= canvas.width());

    for i in 0..canvas.height() - 1 {
        canvas.set_color(i, i, &WHITE_COLOR);
    }
}

pub fn lines(canvas: &mut Canvas) {
    canvas.draw_line(&Point::new(13, 20), &Point::new(80, 40), &WHITE_COLOR);
    canvas.draw_line(&Point::new(20, 13), &Point::new(40, 80), &RED_COLOR);
    canvas.draw_line(&Point::new(80, 40), &Point::new(13, 20), &RED_COLOR);
}

pub fn triangle(canvas: &mut Canvas) {
    canvas.draw_triangle(&Point::new(110, 110),
                         &Point::new(200, 130),
                         &Point::new(290, 260),
                         &RED_COLOR);
}

pub fn wireframe(canvas: &mut Canvas, model: &ObjModelData) {
    assert!(!model.vertices.is_empty());
    assert!(!model.faces.is_empty());

    let x_scale = (canvas.width() - 1) as f32 / 2f32;
    let y_scale = (canvas.height() - 1) as f32 / 2f32;

    let mut draw_line = |a: &Vector3, b: &Vector3| {
        let a = Point::new(((a.x + 1f32) * x_scale) as i32,
                           ((a.y + 1f32) * y_scale) as i32);
        let b = Point::new(((b.x + 1f32) * x_scale) as i32,
                           ((b.y + 1f32) * y_scale) as i32);

        canvas.draw_line(&a, &b, &WHITE_COLOR);
    };

    for f in &model.faces {
        let a = &model.vertices[(f.a - 1) as usize];
        let b = &model.vertices[(f.b - 1) as usize];
        let c = &model.vertices[(f.c - 1) as usize];

        draw_line(a, b);
        draw_line(b, c);
        draw_line(c, a);
    }
}

pub fn random_colored_tris(canvas: &mut Canvas, model: &ObjModelData) {
    use rand::{Rng, StdRng};

    assert!(!model.vertices.is_empty());
    assert!(!model.faces.is_empty());

    let x_scale = (canvas.width() - 1) as f32 / 2f32;
    let y_scale = (canvas.height() - 1) as f32 / 2f32;

    let transform_vertex = |v: &Vector3| -> Point {
        Point::new(((v.x + 1f32) * x_scale) as i32,
                   ((v.y + 1f32) * y_scale) as i32)
    };

    let mut rng = StdRng::new().unwrap();

    let mut draw_triangle = |a: &Vector3, b: &Vector3, c: &Vector3| {
        let a = transform_vertex(&a);
        let b = transform_vertex(&b);
        let c = transform_vertex(&c);
        let clr = rng.next_u32();
        let clr = Color {
            r: (clr & 0xFF) as u8,
            g: ((clr >> 8) & 0xFF) as u8,
            b: ((clr >> 16) & 0xFF) as u8,
            a: 255,
        };
        canvas.draw_triangle(&a, &b, &c, &clr);
    };


    for f in &model.faces {
        let a = &model.vertices[(f.a - 1) as usize];
        let b = &model.vertices[(f.b - 1) as usize];
        let c = &model.vertices[(f.c - 1) as usize];

        draw_triangle(&a, &b, &c);
    }
}

pub fn simple_flat_lit(canvas: &mut Canvas, model: &ObjModelData) {
    assert!(!model.vertices.is_empty());
    assert!(!model.faces.is_empty());

    let x_scale = (canvas.width() - 1) as f32 / 2f32;
    let y_scale = (canvas.height() - 1) as f32 / 2f32;

    let transform_vertex = |v: &Vector3| -> Point {
        Point::new(((v.x + 1f32) * x_scale) as i32,
                   ((v.y + 1f32) * y_scale) as i32)
    };

    let mut draw_triangle = |a: &Vector3, b: &Vector3, c: &Vector3, intensity: f32| {
        assert!(intensity >= 0f32 && intensity <= 1f32);

        let a = transform_vertex(&a);
        let b = transform_vertex(&b);
        let c = transform_vertex(&c);
        let clr = (255f32 * intensity) as u8;
        let clr = Color {
            r: clr,
            g: clr,
            b: clr,
            a: 255,
        };
        canvas.draw_triangle(&a, &b, &c, &clr);
    };

    let light_dir = Vector3::new(0f32, 0f32, 1f32);

    for f in &model.faces {
        let a = model.vertices[(f.a - 1) as usize];
        let b = model.vertices[(f.b - 1) as usize];
        let c = model.vertices[(f.c - 1) as usize];

        let ab = a - b;
        let ac = a - c;
        let normal = Vector3::normalize(Vector3::cross(ab, ac));

        let intensity = Vector3::dot(normal, light_dir);
        if intensity > 0f32 {
            draw_triangle(&a, &b, &c, intensity);
        }
    }
}

pub fn simple_flat_lit_zbuffer(canvas: &mut Canvas, model: &ObjModelData) {
    
}