
pub trait Point2D<T> {
    fn new(x:T, y:T) -> Self;
    fn x(&self) -> T;
    fn y(&self) -> T;
}
