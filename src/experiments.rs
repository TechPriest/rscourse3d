use canvas::*;
use cgmath::prelude::*;
use cgmath::{Vector2, Vector3};
use objparser::ObjModelData;
use buffer::Buffer;
use geometry::point2d::Point2D;

use std::ops::{Add, Mul};

use rayon::prelude::*;

type Vec2f = Vector2<f32>;
type Vec3f = Vector3<f32>;



impl Point2D<i32> for Vec3f {
     fn new(x:i32, y:i32) -> Self {
        Vec3f {
            x: x as f32,
            y: y as f32,
            z: 0f32,
        }
     }

     fn x(&self) -> i32 {
        self.x as i32
     }

     fn y(&self) -> i32 {
        self.y as i32
     }
}


const WHITE_COLOR: Color = Color {
    r: 255,
    g: 255,
    b: 255,
    a: 255,
};

const RED_COLOR: Color = Color {
    r: 255,
    g: 0,
    b: 0,
    a: 255,
};


pub fn per_pixel_line(canvas: &mut Canvas) {
    assert!(canvas.height() <= canvas.width());

    for i in 0..canvas.height() - 1 {
        canvas.set_color(i, i, &WHITE_COLOR);
    }
}

pub fn lines(canvas: &mut Canvas) {
    canvas.draw_line(&Point::new(13, 20), &Point::new(80, 40), &WHITE_COLOR);
    canvas.draw_line(&Point::new(20, 13), &Point::new(40, 80), &RED_COLOR);
    canvas.draw_line(&Point::new(80, 40), &Point::new(13, 20), &RED_COLOR);
}

pub fn triangle(canvas: &mut Canvas) {
    canvas.draw_triangle(&Point::new(110, 110),
                         &Point::new(200, 130),
                         &Point::new(290, 260),
                         &RED_COLOR);
}

pub fn wireframe(canvas: &mut Canvas, model: &ObjModelData) {
    use std::mem::transmute;
    use std::sync::Arc;
    assert!(!model.vertices.is_empty());
    assert!(!model.faces.is_empty());

    struct Context {
        pub x: f32,
        pub y: f32
    };

    let context = Arc::new(Context{ 
        x: (canvas.width() - 1) as f32 / 2f32,
        y: (canvas.height() - 1) as f32 / 2f32,
    });

    fn draw_line(context: &Context, canvas: &mut Canvas,
        a: &Vec3f, b: &Vec3f) 
    {
        let a = Point::new(((a.x + 1f32) * context.x) as i32,
                           ((a.y + 1f32) * context.y) as i32);
        let b = Point::new(((b.x + 1f32) * context.x) as i32,
                           ((b.y + 1f32) * context.y) as i32);

        canvas.draw_line(&a, &b, &WHITE_COLOR);
    }

    unsafe {
        let canvas = canvas as *mut Canvas;
        let hack: usize = transmute(canvas);

        model.faces.par_iter().for_each(|&f|
        {
            let canvas: *mut Canvas = transmute(hack);
            let ref mut canvas = *canvas;
            
            let a = &model.vertices[(f.a - 1) as usize];
            let b = &model.vertices[(f.b - 1) as usize];
            let c = &model.vertices[(f.c - 1) as usize];

            draw_line(&context, canvas, a, b);
            draw_line(&context, canvas, b, c);
            draw_line(&context, canvas, c, a);
        });
    }
}

pub fn random_colored_tris(canvas: &mut Canvas, model: &ObjModelData) {
    use std::sync::{Arc, Mutex};
    use rand::{RngCore, SeedableRng, rngs::StdRng};

    assert!(!model.vertices.is_empty());
    assert!(!model.faces.is_empty());

    struct Context {
        x_scale: f32,
        y_scale: f32,
    };

    impl Context {
        pub fn transform_vertex(&self, v: &Vec3f) -> Point {
            Point::new(((v.x + 1f32) * self.x_scale) as i32,
                    ((v.y + 1f32) * self.y_scale) as i32)
        }
    }

    let context = Arc::new(Context{
        x_scale: (canvas.width() - 1) as f32 / 2f32,
        y_scale: (canvas.height() - 1) as f32 / 2f32,
    });

    impl Into<Color> for u32 {
        fn into(self) -> Color {
            Color {
                r: (self & 0xFF) as u8,
                g: ((self >> 8) & 0xFF) as u8,
                b: ((self >> 16) & 0xFF) as u8,
                a: 255,
            }
        }
    }

    fn draw_triangle(ctx: &Context, canvas: &mut Canvas, a: &Vec3f, b: &Vec3f, c: &Vec3f, clr: &Color) {
        let a = ctx.transform_vertex(&a);
        let b = ctx.transform_vertex(&b);
        let c = ctx.transform_vertex(&c);
        canvas.draw_triangle(&a, &b, &c, clr);
    }

    let rng = Arc::new(Mutex::new(StdRng::from_rng(rand::thread_rng()).unwrap()));

    unsafe {
        use std::mem::transmute;

        let canvas: usize = transmute(canvas as *mut Canvas);
        model.faces.par_iter().for_each(|&f| {
            let canvas: *mut Canvas = transmute(canvas);

            let a = &model.vertices[(f.a - 1) as usize];
            let b = &model.vertices[(f.b - 1) as usize];
            let c = &model.vertices[(f.c - 1) as usize];

            let clr = rng.lock().unwrap().next_u32().into();

            draw_triangle(&context, &mut *canvas, &a, &b, &c, &clr);
        });
    }
}

pub fn simple_flat_lit(canvas: &mut Canvas, model: &ObjModelData) {
    use std::sync::Arc;

    assert!(!model.vertices.is_empty());
    assert!(!model.faces.is_empty());

    struct Context {
        pub x_scale: f32,
        pub y_scale: f32,
        pub light_dir: Vec3f,
    }

    impl Context {
        pub fn transform_vertex(&self, v: &Vec3f) -> Point {
            Point::new(((v.x + 1f32) * self.x_scale) as i32,
                    ((v.y + 1f32) * self.y_scale) as i32)
        }
    }

    let context = Arc::new(Context{
        x_scale: (canvas.width() - 1) as f32 / 2f32,
        y_scale: (canvas.height() - 1) as f32 / 2f32,
        light_dir: Vector3::new(0f32, 0f32, 1f32),
    });

    fn draw_triangle(ctx: &Context, canvas: &mut Canvas, a: &Vec3f, b: &Vec3f, c: &Vec3f, intensity: f32) {
        assert!(intensity >= 0f32 && intensity <= 1f32);

        let a = ctx.transform_vertex(a);
        let b = ctx.transform_vertex(b);
        let c = ctx.transform_vertex(c);
        let ref clr = WHITE_COLOR * intensity;
        canvas.draw_triangle(&a, &b, &c, clr);        
    }

    unsafe {
        use std::mem::transmute;

        let canvas: usize = transmute(canvas as *mut Canvas);

        model.faces.par_iter().for_each(|&f| {
            let canvas: *mut Canvas = transmute(canvas);

            let a = model.vertices[(f.a - 1) as usize];
            let b = model.vertices[(f.b - 1) as usize];
            let c = model.vertices[(f.c - 1) as usize];

            let ab = a - b;
            let ac = a - c;
            let normal = Vector3::<f32>::normalize(Vector3::<f32>::cross(ab, ac));

            let intensity = Vector3::dot(normal, context.light_dir);
            if intensity > 0f32 {
                draw_triangle(&context, &mut *canvas, &a, &b, &c, intensity);
            }
        });
    }
}

fn interpolate<T>(a: T, b: T, c: T, bc: &Vec3f) -> T 
    where T : Add<T, Output = T> + Mul<f32, Output = T> + Clone
{
    (a * bc.x) + (b * bc.y) + (c * bc.z)
}

impl Into<Point> for Vec3f {
    fn into(self) -> Point {
        Point { x: self.x as i32, y: self.y as i32 }
    }
}


pub fn simple_flat_lit_zbuffer(canvas: &mut Canvas, model: &ObjModelData) {
    use std::f32;
    use std::sync::{Arc, Mutex};

    assert!(!model.vertices.is_empty());
    assert!(!model.faces.is_empty());

    struct Context {
        pub x_scale: f32,
        pub y_scale: f32,
    }

    impl Context {
        pub fn transform_vertex(&self, v: &Vec3f) -> Vec3f {
            Vec3f::new((v.x + 1f32) * self.x_scale,
                    (v.y + 1f32) * self.y_scale,
                    v.z)
        }
    }

    let context = Arc::new(Context{
        x_scale: (canvas.width() - 1) as f32 / 2f32,
        y_scale: (canvas.height() - 1) as f32 / 2f32,
    });

    let mut z_buffer = Buffer::<f32>::new(canvas.width(), canvas.height());
    z_buffer.fill(&f32::NEG_INFINITY);

    let z_buffer = Arc::new(Mutex::new(z_buffer));

    fn draw_triangle(canvas: &mut Canvas, z_buffer: &mut Buffer<f32>, 
        a: Vec3f, b: Vec3f, c: Vec3f, intensity: f32) 
    {
        assert!(intensity >= 0f32 && intensity <= 1f32);
        let clr = WHITE_COLOR * intensity;

        canvas.draw_triangle_ex(&a.into() as &Point, &b.into(), &c.into(), |&p, &bc| -> Option<Color> {
            let z = interpolate(a.z, b.z, c.z, &bc);
            let old_z = *z_buffer.get(p.x as usize, p.y as usize);
            if z > old_z {
                z_buffer.set(p.x as usize, p.y as usize, z);
                Some(clr.clone()) 
            } else {
                None
            }
        });
    }

    let light_dir = Vec3f::new(0f32, 0f32, 1f32);

    unsafe {
        use std::mem::transmute;

        let canvas: usize = transmute(canvas as *mut Canvas);

        model.faces.par_iter().for_each(|&f|{
            let canvas: *mut Canvas = transmute(canvas);

            let a = context.transform_vertex(&model.vertices[(f.a - 1) as usize]);
            let b = context.transform_vertex(&model.vertices[(f.b - 1) as usize]);
            let c = context.transform_vertex(&model.vertices[(f.c - 1) as usize]);

            let ab = a - b;
            let ac = a - c;
            let normal = Vector3::normalize(Vector3::cross(ab, ac));

            let intensity = Vector3::dot(normal, light_dir);
            if intensity > 0f32 {
                draw_triangle(&mut *canvas, &mut z_buffer.lock().unwrap(), a, b, c, intensity);
            }
        });
    }
}

pub fn simple_flat_lit_zbuffer_texture(canvas: &mut Canvas, model: &ObjModelData, texture: &Buffer<Color>) {
    use std::f32;
    use std::sync::{Arc, RwLock};

    assert!(!model.vertices.is_empty());
    assert!(!model.faces.is_empty());

    struct Context {
        pub x_scale: f32,
        pub y_scale: f32,
    }

    impl Context {
        pub fn transform_vertex(&self, v: &Vec3f) -> Vec3f {
            Vec3f::new((v.x + 1f32) * self.x_scale,
                        (v.y + 1f32) * self.y_scale,
                        v.z)
        }
    }

    let context = Arc::new(Context{
        x_scale: (canvas.width() - 1) as f32 / 2f32,
        y_scale: (canvas.height() - 1) as f32 / 2f32,
    });

    let mut z_buffer = Buffer::<f32>::new(canvas.width(), canvas.height());
    z_buffer.fill(&f32::NEG_INFINITY);

    let z_buffer = Arc::new(RwLock::new(z_buffer));
    let texture = Arc::new(texture);

    fn draw_triangle(canvas: &mut Canvas, z_buffer: &RwLock<Buffer<f32>>, texture: &Buffer<Color>,
        a: Vec3f, a_uv: Vec2f,
        b: Vec3f, b_uv: Vec2f,
        c: Vec3f, c_uv: Vec2f,
        intensity: f32)
    {
        assert!(intensity >= 0f32 && intensity <= 1f32);

        canvas.draw_triangle_ex(&a.into() as &Point, &b.into(), &c.into(), |&p, &bc| -> Option<Color> {
            let z = interpolate(a.z, b.z, c.z, &bc);
            let old_z = { 
                *z_buffer.read().unwrap().get(p.x as usize, p.y as usize) 
            };
            if z > old_z {
                let z_buffer = &mut z_buffer.write().unwrap();
                let old_z = *z_buffer.get(p.x as usize, p.y as usize);  // Double-checking to make sure nothing changed during relock
                if z > old_z {
                    z_buffer.set(p.x as usize, p.y as usize, z);

                    let uv = interpolate(a_uv, b_uv, c_uv, &bc);
                    let tex_x = (((texture.width() - 1) as f32) * uv.x) as usize;
                    let tex_y = (((texture.height() - 1) as f32) * uv.y) as usize;

                    let tex_color = texture.get(tex_x, texture.height() - tex_y) * intensity;
                    Some(tex_color) 
                } else {
                    None
                }
            } else {
                None
            }
        });
    }


    let light_dir = Vector3::new(0f32, 0f32, 1f32);

    unsafe {
        use std::mem::transmute;

        let canvas: usize = transmute(canvas as *mut Canvas);

        model.faces.par_iter().for_each(|&f|{
            let canvas: *mut Canvas = transmute(canvas);

            let a = context.transform_vertex(&model.vertices[(f.a - 1) as usize]);
            let b = context.transform_vertex(&model.vertices[(f.b - 1) as usize]);
            let c = context.transform_vertex(&model.vertices[(f.c - 1) as usize]);

            let ab = a - b;
            let ac = a - c;
            let normal = Vector3::normalize(Vector3::cross(ab, ac));

            let intensity = Vector3::dot(normal, light_dir);
            if intensity > 0f32 {
                let a_uv = model.texcoords[(f.a_uv - 1) as usize];
                let b_uv = model.texcoords[(f.b_uv - 1) as usize];
                let c_uv = model.texcoords[(f.c_uv - 1) as usize];

                draw_triangle(&mut *canvas, &z_buffer, &texture, a, a_uv, b, b_uv, c, c_uv, intensity);
            }
        });
    }
}

pub fn simple_smooth_lit_zbuffer_texture(canvas: &mut Canvas, model: &ObjModelData, texture: &Buffer<Color>) {
    use std::f32::NEG_INFINITY;

    assert!(!model.vertices.is_empty());
    assert!(!model.faces.is_empty());

    let x_scale = (canvas.width() - 1) as f32 / 2f32;
    let y_scale = (canvas.height() - 1) as f32 / 2f32;

    let mut z_buffer = Buffer::<f32>::new(canvas.width(), canvas.height());
    z_buffer.fill(&NEG_INFINITY);

    let transform_vertex = |v: &Vec3f| -> Vec3f {
        Vector3::new((v.x + 1f32) * x_scale,
                     (v.y + 1f32) * y_scale,
                       v.z)
    };

    let light_dir = Vector3::new(0f32, 0f32, 1f32);

    let mut draw_triangle = |a: &Vec3f, a_uv: &Vector2<f32>, a_norm: &Vec3f,
                             b: &Vec3f, b_uv: &Vector2<f32>, b_norm: &Vec3f,
                             c: &Vec3f, c_uv: &Vector2<f32>, c_norm: &Vec3f| 
    {
        use std::f32;

        let a = transform_vertex(&a);
        let b = transform_vertex(&b);
        let c = transform_vertex(&c);

        canvas.draw_triangle_ex(&a.into() as &Point, &b.into(), &c.into(), |&p, &bc| -> Option<Color> {
            let z = interpolate(a.z, b.z, c.z, &bc);
            let old_z = *z_buffer.get(p.x as usize, p.y as usize);
            if z > old_z {
                z_buffer.set(p.x as usize, p.y as usize, z);

                let uv = interpolate(*a_uv, *b_uv, *c_uv, &bc);
                let tex_x = (((texture.width() - 1) as f32) * uv.x) as usize;
                let tex_y = (((texture.height() - 1) as f32) * uv.y) as usize;
                let tex_color = texture.get(tex_x, texture.height() - tex_y);

                let norm = interpolate(*a_norm, *b_norm, *c_norm, &bc);
                let intensity = Vector3::dot(norm, light_dir);
                let intensity = f32::max(0f32, f32::min(1f32, intensity));

                Some(tex_color * intensity)
            } else {
                None
            }
        });
    };

    for f in &model.faces {
        let a = model.vertices[(f.a - 1) as usize];
        let b = model.vertices[(f.b - 1) as usize];
        let c = model.vertices[(f.c - 1) as usize];

        let ab = a - b;
        let ac = a - c;

        let normal = Vector3::<f32>::normalize(Vector3::<f32>::cross(ab, ac));

        let intensity = Vector3::<f32>::dot(normal, light_dir);
        if intensity > 0f32 {
            let ref a_uv = model.texcoords[(f.a_uv - 1) as usize];
            let ref b_uv = model.texcoords[(f.b_uv - 1) as usize];
            let ref c_uv = model.texcoords[(f.c_uv - 1) as usize];

            let ref a_norm = model.normals[(f.a_norm - 1) as usize];
            let ref b_norm = model.normals[(f.b_norm - 1) as usize];
            let ref c_norm = model.normals[(f.c_norm - 1) as usize];

            draw_triangle(&a, a_uv, a_norm,
                        &b, b_uv, b_norm,
                        &c, c_uv, c_norm);
        }
    }
}

pub fn simple_smooth_lit_zbuffer_texture_persp(canvas: &mut Canvas, model: &ObjModelData, texture: &Buffer<Color>) {
    use std::f32::NEG_INFINITY;
    use cgmath::{ Deg, perspective, Point3, Vector4, Matrix4 };
    
    assert!(!model.vertices.is_empty());
    assert!(!model.faces.is_empty());
    
    let x_scale = (canvas.width() - 1) as f32 / 2f32;
    let y_scale = (canvas.height() - 1) as f32 / 2f32;

    let aspect = canvas.width() as f32 / canvas.height() as f32;

    let world = Matrix4::<f32>::identity();
    let (view, view_vector) = { 
        let camera_pos = Point3::<f32>::new(-1f32, 1.25f32, -2.5f32);
        let camera_target_pos = Point3::<f32>::new(0f32, 0f32, 0f32);
        let up = Vector3::<f32>::new(0f32, 1f32, 0f32);

        (Matrix4::<f32>::look_at(camera_pos, camera_target_pos, up), 
         Vector3::<f32>::new(0f32, 0f32, -1f32))
    };
    let proj = perspective(Deg(90f32), aspect, 0.1f32, 10f32);

    let wvp = world * view * proj;


    let mut z_buffer = Buffer::<f32>::new(canvas.width(), canvas.height());
    z_buffer.fill(&NEG_INFINITY);

    let transform_vertex = |v: &Vec3f| -> Vec3f {
        let v = Vector4::<f32>::new(v.x, v.y, v.z, 1f32);
        let v = wvp * v;
        Vector3::new((v.x + 1f32) * x_scale,
                (v.y + 1f32) * y_scale,
                v.z)
    };

    let light_dir = Vector3::new(0f32, 0f32, 1f32);

    let mut draw_triangle = |a: &Vec3f, a_uv: &Vector2<f32>, a_norm: &Vec3f,
                             b: &Vec3f, b_uv: &Vector2<f32>, b_norm: &Vec3f,
                             c: &Vec3f, c_uv: &Vector2<f32>, c_norm: &Vec3f| 
    {
        use std::f32;

        let a = transform_vertex(&a);
        let b = transform_vertex(&b);
        let c = transform_vertex(&c);

        // view-based backface culling
        let ab = a - b;
        let ac = a - c;
        let normal = Vector3::<f32>::normalize(Vector3::<f32>::cross(ab, ac));
        let intensity = Vector3::<f32>::dot(normal, view_vector);

        if intensity > 0f32 {
            canvas.draw_triangle_ex(&a.into() as &Point, &b.into(), &c.into(), |&p, &bc| -> Option<Color> {
                let z = interpolate(a.z, b.z, c.z, &bc);
                let old_z = *z_buffer.get(p.x as usize, p.y as usize);
                if z > old_z {
                    z_buffer.set(p.x as usize, p.y as usize, z);

                    let uv = interpolate(*a_uv, *b_uv, *c_uv, &bc);
                    let tex_x = (((texture.width() - 1) as f32) * uv.x) as usize;
                    let tex_y = (((texture.height() - 1) as f32) * uv.y) as usize;
                    let tex_color = texture.get(tex_x, texture.height() - tex_y);

                    let norm = interpolate(*a_norm, *b_norm, *c_norm, &bc);
                    let intensity = Vector3::dot(norm, light_dir);
                    let intensity = f32::max(0f32, f32::min(1f32, intensity));

                    Some(tex_color * intensity)
                } else {
                    None
                }
            });
        }
    };

    for f in &model.faces {
        let a = model.vertices[(f.a - 1) as usize];
        let b = model.vertices[(f.b - 1) as usize];
        let c = model.vertices[(f.c - 1) as usize];

        let ref a_uv = model.texcoords[(f.a_uv - 1) as usize];
        let ref b_uv = model.texcoords[(f.b_uv - 1) as usize];
        let ref c_uv = model.texcoords[(f.c_uv - 1) as usize];

        let ref a_norm = model.normals[(f.a_norm - 1) as usize];
        let ref b_norm = model.normals[(f.b_norm - 1) as usize];
        let ref c_norm = model.normals[(f.c_norm - 1) as usize];

        draw_triangle(&a, a_uv, a_norm,
                    &b, b_uv, b_norm,
                    &c, c_uv, c_norm);
    }
}