#![allow(dead_code)]

use std::io::prelude::*;
use std::str::FromStr;
use std::error::Error as StdError;
use std::default::Default;
use std::convert::From;
use cgmath::{Vector2, Vector3};
use super::mesh::Face;

error_chain! {
    errors{ InvalidFormat }
}

pub struct ObjModelData {
    pub vertices: Vec<Vector3<f32>>,
    pub texcoords: Vec<Vector2<f32>>,
    pub normals: Vec<Vector3<f32>>,
    pub faces: Vec<Face>,
}

const COMMENT_PREFIX: &'static str = "#";
const VERTEX_PREFIX: &'static str = "v ";
const FACE_PREFIX: &'static str = "f ";
const NORMAL_PREFIX: &'static str = "vn ";
const TEXCOORD_PREFIX: &'static str = "vt ";

/// Checks if specified line from OBJ file is a comment.
fn is_comment(s: &str) -> bool {
    &s[0..COMMENT_PREFIX.len()] == COMMENT_PREFIX
}

/// Extracts substring with vertex coordinates only, without prefix.
fn get_vertex_coord_substr(s: &str) -> &str {
    &s[VERTEX_PREFIX.len()..s.len()]
}

/// Extracts substring with texture coordinates only, without prefix
fn get_texcoord_data_substr(s: &str) -> &str {
    &s[TEXCOORD_PREFIX.len()..s.len()]
}

/// Extracts substring with normal coordinates only, without prefix
fn get_normal_data_substr(s: &str) -> &str {
    &s[NORMAL_PREFIX.len()..s.len()]
}

/// Extracts substring with face data only, without prefix
fn get_face_data_substr(s: &str) -> &str {
    &s[FACE_PREFIX.len()..s.len()]
}

#[test]
fn detects_comment() {
    let input = "#v 12.34 42.42 66.99";
    assert!(is_comment(&input));
    let input = "v 12.34 42.42 66.99";
    assert!(!is_comment(&input));
}

pub fn parse_obj<R: BufRead>(src: R) -> Result<ObjModelData> {
    // https://en.wikipedia.org/wiki/Wavefront_.obj_file

    let mut vertices: Vec<Vector3<f32>> = Default::default();
    let mut texcoords: Vec<Vector2<f32>> = Default::default();
    let mut normals: Vec<Vector3<f32>> = Default::default();
    let mut faces: Vec<Face> = Default::default();

    for line in src.lines() {
        let line = match line {
            Ok(line) => line,
            Err(_) => break,
        };

        if line.len() == 0 {
            continue;
        }

        if is_comment(&line) {
            continue;
        }

        match &line[0..2] {
            VERTEX_PREFIX => {
                vertices.push(parse_coord(get_vertex_coord_substr(&line)).unwrap());
            }
            FACE_PREFIX => faces.push(parse_face(get_face_data_substr(&line)).unwrap()),
            "g " => (),
            "s " => (),
            _ => {
                match &line[0..3] {
                    TEXCOORD_PREFIX => {
                        let texcoord = parse_texcoord(get_texcoord_data_substr(&line)).unwrap();
                        texcoords.push(texcoord);
                    },
                    NORMAL_PREFIX => {
                        let normal = parse_coord(get_normal_data_substr(&line)).unwrap();
                        normals.push(normal);
                    },
                    "vp " => (),
                    _ => panic!("Unexpected line format {:?} {:?}", line, &line[0..3]),
                }
            }
        };
    }

    Ok(ObjModelData {
        vertices: vertices,
        texcoords: texcoords,
        normals: normals,
        faces: faces,
    })
}

/// Returns first non-empty substring
fn get_next_nonempty_substr<'a>(iter: &mut dyn Iterator<Item = &'a str>) -> Option<&'a str> {
    loop {
        match iter.next() {
            None => return None,
            Some(s) => {
                if s.len() > 0 {
                    return Some(s);
                }
            }
        }
    }
}

/// Parses next float value in input stream
fn parse_next<T: FromStr>(iter: &mut dyn Iterator<Item = &str>) -> Result<T>
    where T::Err: StdError
{
    let s = match get_next_nonempty_substr(iter) {
        None => bail!(ErrorKind::InvalidFormat),
        Some(s) => s,
    };
    Ok(T::from_str(s).unwrap())
}

/// Parses vertex definition in text OBJ format.
fn parse_coord(data: &str) -> Result<Vector3<f32>> {
    // Input example "0.123 0.234 0.345 1.0"
    let mut iter = data.split_whitespace();
    Ok(Vector3 {
        x: parse_next::<f32>(&mut iter)?,
        y: parse_next::<f32>(&mut iter)?,
        z: parse_next::<f32>(&mut iter)?,
    })
}

/// Parses UV-coordinate for vertex
fn parse_texcoord(data: &str) -> Result<Vector2<f32>> {
    // Input example "0.535 0.917 0.000"
    let mut iter = data.split_whitespace();
    Ok(Vector2 {
        x: parse_next::<f32>(&mut iter)?,
        y: parse_next::<f32>(&mut iter)?,
    })
}

#[derive(Default, Debug, Copy, Clone)]
struct FaceIndices {
    coord: i32,
    tex_coord: i32,
    normal: i32,
}

fn parse_face_indices(s: &str) -> Result<FaceIndices> {
    // Imput example "460/450/460"
    let mut iter = s.split('/');
    Ok(FaceIndices {
        coord: parse_next::<i32>(&mut iter)?,
        tex_coord: parse_next::<i32>(&mut iter)?,
        normal: parse_next::<i32>(&mut iter)?,
    })
}

#[test]
fn parses_face_indices() {
    let input = "460/450/460";
    let f = parse_face_indices(&input).unwrap();
    assert!(f.coord == 460);
    assert!(f.tex_coord == 450);
    assert!(f.normal == 460);
}


/// Parses face indices
fn parse_face(data: &str) -> Result<Face> {
    // Imput example "460/450/460 260/242/260 29/244/29"
    let mut iter = data.split_whitespace();

    let parse_next_face_indices =
        |mut iter: &mut dyn Iterator<Item = &str>| -> Result<FaceIndices> {
            let s = match get_next_nonempty_substr(&mut iter) {
                None => bail!(ErrorKind::InvalidFormat),
                Some(s) => s
            };
            let f = parse_face_indices(s)?;
            Ok(f)
        };

    let a = parse_next_face_indices(&mut iter)?;
    let b = parse_next_face_indices(&mut iter)?;
    let c = parse_next_face_indices(&mut iter)?;

    Ok(Face {
        a: a.coord,
        b: b.coord,
        c: c.coord,
        
        a_uv: a.tex_coord,
        b_uv: b.tex_coord,
        c_uv: c.tex_coord,

        a_norm: a.normal,
        b_norm: b.normal,
        c_norm: c.normal,
    })
}

#[test]
fn parses_face_data() {
    let input = "460/450/460 260/242/260 29/244/29";
    let f = parse_face(input).unwrap();

    assert!(f.a == 460);
    assert!(f.b == 260);
    assert!(f.c == 29);
}

#[test]
fn parses_vertex() {
    let input = "v   -0.000581696 -0.734665 -0.623267";
    let v = parse_coord(get_vertex_coord_substr(&input)).unwrap();

    assert!(v.x == -0.000581696f32);
    assert!(v.y == -0.734665f32);
    assert!(v.z == -0.623267f32);
}

#[test]
fn parses_normal() {
    let input = "vn  0.001 0.482 -0.876";
    let n = parse_coord(get_normal_data_substr(&input)).unwrap();

    assert!(n.x == 0.001);
    assert!(n.y == 0.482);
    assert!(n.z == -0.876);
}

#[test]
fn fails_to_parse_coord_on_insufficient_data() {
    let input = "v 12.34 42.42";
    let r = parse_coord(get_vertex_coord_substr(&input));
    assert!(r.is_err());
}

#[test]
fn fails_to_parse_coord_on_malformed_data() {
    let input = "v zzz1sd2.34 xxx42.42 yyy66.99";
    let r = parse_coord(get_vertex_coord_substr(&input));
    assert!(r.is_err());
}

#[test]
fn parses_texture_coord() {
    let input = "vt  0.535 0.917 0.000";
    let uv = parse_texcoord(get_texcoord_data_substr(&input)).unwrap();

    assert!(uv.x == 0.535);
    assert!(uv.y == 0.917);
}
