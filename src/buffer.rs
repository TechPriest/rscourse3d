#![allow(dead_code)]

use std::vec::Vec;
use std::default::Default;


#[derive(Debug)]
pub struct Buffer<T> {
    values: Vec<T>,
    width: usize,
    height: usize,
}

impl<T> Buffer<T>
    where T: Default + Clone
{
    pub fn new(width: usize, height: usize) -> Buffer<T> {
        Buffer {
            values: vec![Default::default(); width * height],
            width: width,
            height: height,
        }
    }

    #[inline]
    pub fn width(&self) -> usize {
        self.width
    }

    #[inline]
    pub fn height(&self) -> usize {
        self.height
    }

    #[inline]
    fn value_offset(&self, x: usize, y: usize) -> usize {
        assert!(x < self.width());
        assert!(y < self.height());

        ((self.height() - 1 - y) * self.width) + x
    }

    #[inline]
    pub fn get(&self, x: usize, y: usize) -> &T {
        let offset = self.value_offset(x, y);
        &self.values[offset]
    }

    #[inline]
    pub fn set(&mut self, x: usize, y: usize, v: T) {
        let offset = self.value_offset(x, y);
        self.values[offset] = v;
    }

    pub fn fill(&mut self, v: &T) {
        for i in 0..self.values.len() - 1 {
            self.values[i] = v.clone();
        }
    }

    pub fn data(&self) -> &[T] {
        self.values.as_slice()
    }
}
