use cgmath::{Vector2, Vector3};

#[derive(Copy, Clone, Debug)]
pub struct Vertex {
    pub coord: Vector3<f32>,
    pub uv: Vector2<f32>,
    pub normal: Vector3<f32>,
}

#[derive(Default, Copy, Clone, Debug)]
pub struct Face {
    pub a: i32,
    pub b: i32,
    pub c: i32,

    pub a_uv: i32,
    pub b_uv: i32,
    pub c_uv: i32,

    pub a_norm: i32,
    pub b_norm: i32,
    pub c_norm: i32,
}
