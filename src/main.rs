#![feature(const_fn)]
#![recursion_limit = "1024"]


extern crate byteorder;
extern crate time;
extern crate rand;
extern crate png;
extern crate cgmath;

#[macro_use]
extern crate error_chain;

extern crate rayon;

mod buffer;
mod canvas;
mod geometry;
mod mesh;
mod objparser;
mod experiments;

use std::io::{BufReader, BufWriter, Read};
use canvas::{Color, Canvas};
use buffer::Buffer;

mod errors {
    error_chain!{}
}

use errors::*;


const CLEAR_COLOR: Color = Color {
    r: 50,
    g: 50,
    b: 50,
    a: 255,
};

fn load_texture<R>(reader: &mut R) -> Result<Buffer<Color>> 
    where R: Read
{
    use png::ColorType::*;

    let decoder = png::Decoder::new(reader);
    let (info, mut reader) = decoder.read_info().chain_err(|| "Unable to read PNG info")?;
    let mut buf = vec![0; info.buffer_size()];
    reader.next_frame(&mut buf).chain_err(|| "Unable to read PNG frame")?;

    let mut texture = Buffer::<Color>::new(info.width as usize, info.height as usize);

    // https://github.com/PistonDevelopers/image-png/blob/master/examples/show.rs
    match info.color_type {
        RGBA => {
            for y in 0 .. info.height - 1 {
                let line_offset = (y as usize) * info.line_size;
                for x in 0 .. info.width - 1 {
                    let pixel_offset = line_offset + (x * 4) as usize;
                    let clr = Color {
                        r: buf[pixel_offset + 0],
                        g: buf[pixel_offset + 1],
                        b: buf[pixel_offset + 2],
                        a: buf[pixel_offset + 3],
                    };
                    texture.set(x as usize, y as usize, clr);
                }                
            }
        },
        _ => unreachable!("Unexpected color type"),
    }

    Ok(texture)
}

fn run_experiment<F>(experiment: F, image_width: usize, image_height: usize, name: &str)
    where F: Fn(&mut Canvas)
{
    use std::sync::atomic::{AtomicUsize, Ordering};
    use time::OffsetDateTime;

    static COUNTER: AtomicUsize = AtomicUsize::new(0);

    let mut canvas = Canvas::new(image_width, image_height);
    canvas.clear_color(&CLEAR_COLOR);

    let start_time = OffsetDateTime::now();
    experiment(&mut canvas);
    let end_time = OffsetDateTime::now();

    println!("{0} finished in {1}s", name, (end_time - start_time).as_seconds_f64());

    let n = COUNTER.fetch_add(1, Ordering::SeqCst) + 1;

    let file_name = format!("{0:02}_{1}.png", n, name);
    let f = std::fs::File::create(file_name).unwrap();
    let ref mut writer = BufWriter::new(f);
    let mut encoder = png::Encoder::new(writer, canvas.width() as u32, canvas.height() as u32);
    encoder.set_color(png::ColorType::RGBA);
    encoder.set_depth(png::BitDepth::Eight);
    let mut png_writer = encoder.write_header().unwrap();

    let bytes = unsafe {
        let pixel_data = &canvas.color_data()[0] as *const _ as *const u8;
        std::slice::from_raw_parts(pixel_data, canvas.width() * canvas.height() * 4)
    };
    png_writer.write_image_data(&bytes).unwrap();
}

fn run_experiment_1<F, A>(experiment: F,
                          a: &A,
                          image_width: usize,
                          image_height: usize,
                          name: &str)
    where F: Fn(&mut Canvas, &A)
{
    run_experiment(|mut canvas| {
                       experiment(&mut canvas, &a);
                   },
                   image_width,
                   image_height,
                   name);
}

fn run_experiment_2<F, A1, A2>(experiment: F,
                          a1: &A1,
                          a2: &A2,
                          image_width: usize,
                          image_height: usize,
                          name: &str)
    where F: Fn(&mut Canvas, &A1, &A2)
{
    run_experiment(|mut canvas| {
                       experiment(&mut canvas, &a1, &a2);
                   },
                   image_width,
                   image_height,
                   name);
}

fn run() -> Result<()> {
    run_experiment(experiments::per_pixel_line,
                   200usize,
                   200usize,
                   "per_pixel_line");

    run_experiment(experiments::lines, 200usize, 200usize, "lines");

    run_experiment(experiments::triangle, 300usize, 300usize, "triangle");

    let ref mut f = std::fs::File::open("african_head_diffuse.png").chain_err(|| "Failed to open diffuse texture file")?;
    let texture = load_texture(f).chain_err(|| "Failed to load diffuse texture")?;
    let f = std::fs::File::open("african_head.obj").chain_err(|| "Failed to open model file")?;
    let model = objparser::parse_obj(BufReader::new(f)).chain_err(|| "Failed to load model data")?;

    run_experiment_1(experiments::wireframe,
                     &model,
                     800usize,
                     800usize,
                     "wireframe");

    run_experiment_1(experiments::random_colored_tris,
                     &model,
                     800usize,
                     800usize,
                     "random_colored_tris");

    run_experiment_1(experiments::simple_flat_lit,
                     &model,
                     800usize,
                     800usize,
                     "simple_flat_lit");

    run_experiment_1(experiments::simple_flat_lit_zbuffer,
                     &model,
                     800usize,
                     800usize,
                     "simple_flat_lit_zbuffer");

    run_experiment_2(experiments::simple_flat_lit_zbuffer_texture,
                     &model,
                     &texture,
                     800usize,
                     800usize,
                     "simple_flat_lit_zbuffer_texture");   

    run_experiment_2(experiments::simple_smooth_lit_zbuffer_texture,
                     &model,
                     &texture,
                     800usize,
                     800usize,
                     "simple_smooth_lit_zbuffer_texture");

    run_experiment_2(experiments::simple_smooth_lit_zbuffer_texture_persp,
                     &model,
                     &texture,
                     800usize,
                     800usize,
                     "simple_smooth_lit_zbuffer_texture_persp");

    Ok(())
}

quick_main!{run}