#![allow(dead_code)]

use std::cmp::{PartialEq, Eq, min, max};
use std::ops::{Index, IndexMut, Sub, Mul};
use std::default::Default;
use std::marker::Sized;
use buffer::Buffer;

use geometry::point2d::Point2D;
use cgmath::Vector3;

#[derive(Default, Copy, Clone, Debug)]
pub struct Point {
    pub x: i32,
    pub y: i32,
}

impl Point {
    pub fn new(x: i32, y: i32) -> Point {
        Point { x: x, y: y }
    }
}

impl PartialEq for Point {
    fn eq(&self, other: &Point) -> bool {
        self.x == other.x && self.y == other.y
    }

    fn ne(&self, other: &Point) -> bool {
        !self.eq(other)
    }
}

impl Eq for Point {}

impl Index<i32> for Point {
    type Output = i32;

    fn index<'a>(&'a self, ndx: i32) -> &'a i32 {
        match ndx {
            0 => &self.x,
            1 => &self.y,
            _ => panic!("Invalid index {0}", ndx),
        }
    }
}

impl IndexMut<i32> for Point {
    fn index_mut<'a>(&'a mut self, ndx: i32) -> &'a mut i32 {
        match ndx {
            0 => &mut self.x,
            1 => &mut self.y,
            _ => panic!("Invalid index {0}", ndx),
        }
    }
}

impl Point2D<i32> for Point {
    fn new(x:i32, y:i32) -> Point {
        Point{
            x: x,
            y: y,
        }
    }

    fn x(&self) -> i32 {
        self.x
    }

    fn y(&self) -> i32 {
        self.y
    }
}

pub trait F32Cast {
    fn to_f32(&self) -> f32;
}

impl F32Cast for f32 {
    fn to_f32(&self) -> f32 {
        *self
    }
}

impl F32Cast for i32 {
    fn to_f32(&self) -> f32 {
        *self as f32
    }
}

pub trait I32Cast {
    fn to_i32(&self) -> i32;
}

impl I32Cast for f32 {
    fn to_i32(&self) -> i32 {
        *self as i32
    }
}

impl I32Cast for i32 {
    fn to_i32(&self) -> i32 {
        *self
    }
}

#[derive(Debug, Clone, Default)]
#[repr(C)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl<'a> Mul<f32> for &'a Color {
    type Output = Color;

    fn mul(self, b: f32) -> Color {
        use std::f32;

        let b = f32::max(0f32, f32::min(1f32, b));
        Color {
            r: ((self.r as f32) * b) as u8,
            g: ((self.g as f32) * b) as u8,
            b: ((self.b as f32) * b) as u8,
            a: self.a,  // Leave alpha alone for now
        }
    }
}

impl Mul<f32> for Color {
    type Output = Self;

    fn mul(self, b: f32) -> Self {
        &self * b
    }
}

#[test]
fn correctly_multiplies_color_by_f32() {
    let a = Color{
        r: 255, 
        g: 255,
        b: 255,
        a: 255,
    };
    let b = 0.5f32;
    let c = a * b;

    assert!(c.r == 127);
    assert!(c.g == 127);
    assert!(c.b == 127);
    assert!(c.a == 255);
}


type ColorBuffer = Buffer<Color>;
type DepthBuffer = Buffer<f32>;

pub struct Canvas {
    color_buffer: ColorBuffer,
    depth_buffer: DepthBuffer,
}

#[allow(dead_code)]
impl Canvas {
    pub fn new(width: usize, height: usize) -> Canvas {
        Canvas {
            color_buffer: ColorBuffer::new(width, height),
            depth_buffer: DepthBuffer::new(width, height),
        }
    }

    pub fn width(&self) -> usize {
        assert!(self.color_buffer.width() == self.depth_buffer.width());
        self.color_buffer.width()
    }

    pub fn height(&self) -> usize {
        assert!(self.color_buffer.height() == self.depth_buffer.height());
        self.color_buffer.height()
    }

    pub fn color_data(&self) -> &[Color] {
        self.color_buffer.data()
    }

    pub fn depth_data(&self) -> &[f32] {
        self.depth_buffer.data()
    }

    pub fn color(&self, x: usize, y: usize) -> &Color {
        self.color_buffer.get(x, y)
    }

    pub fn clear_color(&mut self, clr: &Color) {
        self.color_buffer.fill(clr);
    }

    pub fn clear_depth(&mut self, depth: f32) {
        self.depth_buffer.fill(&depth);
    }

    pub fn set_color(&mut self, x: usize, y: usize, color: &Color) {
        self.color_buffer.set(x, y, color.clone());
    }

    pub fn depth(&self, x: usize, y: usize) -> f32 {
        *self.depth_buffer.get(x, y)
    }

    pub fn set_depth(&mut self, x: usize, y: usize, depth: f32) {
        self.depth_buffer.set(x, y, depth);
    }

    pub fn draw_line(&mut self, a: &Point, b: &Point, clr: &Color) {
        // https://habrahabr.ru/post/248153/
        // TODO: Draw AA lines with https://en.wikipedia.org/wiki/Xiaolin_Wu%27s_line_algorithm
        let steep = (a.x - b.x).abs() < (a.y - b.y).abs();

        let (x0, y0, x1, y1) = {
            let (ax, ay, bx, by) = if steep {
                (a.y, a.x, b.y, b.x)
            } else {
                (a.x, a.y, b.x, b.y)
            };

            if ax > bx {
                (bx, by, ax, ay)
            } else {
                (ax, ay, bx, by)
            }
        };

        let dx = x1 - x0;
        let dy = y1 - y0;
        let derror2 = dy.abs() * 2;
        let mut error2 = 0;
        let mut y = y0;

        for x in x0..(x1 + 1) {
            {
                let (x, y) = if steep { (y, x) } else { (x, y) };
                self.set_color(x as usize, y as usize, clr);
            };
            error2 += derror2;
            if error2 > dx {
                y += if y1 > y0 { 1 } else { -1 };
                error2 -= dx * 2;
            }
        }
    }

    fn barycentric_coord<T, Tp, V>(a: &T, b: &T, c: &T, x: &Tp) -> Vector3<f32>
        where T: Point2D<V>,
              Tp: Point2D<V>,  
              V: Sub<V, Output = V> + F32Cast
    {
        // SEE: https://github.com/ssloy/tinyrenderer/blob/master/our_gl.cpp
        // Vec3f barycentric(Vec2i *pts, Vec2i P) {
        // Vec3f u = cross(Vec3f(pts[2][0]-pts[0][0], pts[1][0]-pts[0][0], pts[0][0]-P[0]),
        //      Vec3f(pts[2][1]-pts[0][1], pts[1][1]-pts[0][1], pts[0][1]-P[1]));
        // if (std::abs(u[2])<1) return Vec3f(-1,1,1); // triangle is degenerate, in this case return smth with negative coordinates
        //  return Vec3f(1.f-(u.x+u.y)/u.z, u.y/u.z, u.x/u.z);
        // }
        //
        const DEGENERATE_TRIANGLE_BC: Vector3<f32> = Vector3::<f32> {
            x: -1f32,
            y: -1f32,
            z: -1f32,
        };

        let u1 = Vector3::new((c.x() - a.x()).to_f32(),
                              (b.x() - a.x()).to_f32(),
                              (a.x() - x.x()).to_f32());
        let u2 = Vector3::new((c.y() - a.y()).to_f32(),
                              (b.y() - a.y()).to_f32(),
                              (a.y() - x.y()).to_f32());
        let u = Vector3::cross(u1, u2);

        if u.z.abs() < 1.0f32 {
            return DEGENERATE_TRIANGLE_BC;
        } else {
            return Vector3::new(1.0f32 - (u.x + u.y) / u.z, u.y / u.z, u.x / u.z);
        }
    }

    pub fn draw_triangle_ex<P, PsFn>(&mut self, a: &P, b: &P, c: &P, mut ps: PsFn)
        where P: Point2D<i32> + Index<i32> + IndexMut<i32> + Sized,
            <P as Index<i32>>::Output: Sized + Default + Clone + I32Cast,
            PsFn: FnMut(&Point, &Vector3<f32>) -> Option<Color>
    {
        // https://habrahabr.ru/post/248159/
        // https://habrahabr.ru/post/249467/ - based on barycentric coords.

        // Vec2i bboxmin(image.get_width()-1,  image.get_height()-1);
        // Vec2i bboxmax(0, 0);
        // Vec2i clamp(image.get_width()-1, image.get_height()-1);
        // for (int i=0; i<3; i++) {
        //  for (int j=0; j<2; j++) {
        //      bboxmin[j] = std::max(0,        std::min(bboxmin[j], pts[i][j]));
        //      bboxmax[j] = std::min(clamp[j], std::max(bboxmax[j], pts[i][j]));
        //  }
        // }
        // Vec2i P;
        // for (P.x=bboxmin.x; P.x<=bboxmax.x; P.x++) {
        //  for (P.y=bboxmin.y; P.y<=bboxmax.y; P.y++) {
        //      Vec3f bc_screen  = barycentric(pts, P);
        //      if (bc_screen.x<0 || bc_screen.y<0 || bc_screen.z<0) continue;
        //          image.set(P.x, P.y, color);
        //      }
        // }
        //
        let pts = [&a, &b, &c];
        let mut bbox_min = Point::new((self.width() - 1) as i32, (self.height() - 1) as i32);
        let mut bbox_max = Point::new(0, 0);
        let clamp = Point::new((self.width() - 1) as i32, (self.height() - 1) as i32);

        for i in 0..3 {
            for j in 0..2 {
                bbox_min[j] = max(0, min(bbox_min[j], pts[i][j].to_i32()));
                bbox_max[j] = min(clamp[j], max(bbox_max[j], pts[i][j].to_i32()));
            }
        }

        for x in bbox_min.x..bbox_max.x + 1 {
            for y in bbox_min.y..bbox_max.y + 1 {
                let p = Point::new(x, y);
                let bc = Canvas::barycentric_coord(a, b, c, &p);
                if bc.x >= 0f32 && bc.y >= 0f32 && bc.z >= 0f32 {
                    match ps(&p, &bc) {
                        Some(clr) => self.set_color(x as usize, y as usize, &clr),
                        _ => (),
                    }
                }
            }
        }
    }

    pub fn draw_triangle(&mut self, a: &Point, b: &Point, c: &Point, clr: &Color) {
        self.draw_triangle_ex(a, b, c, |_, _| Some(clr.clone()));
    }
}
